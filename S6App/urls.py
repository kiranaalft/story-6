from django.urls import path
from . import views

app_name = 'S6App'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('books/', views.books, name='books'),
    path('books/getJson/<str:query>', views.getJson, name='getJson'),
    path('login/', views.loginView, name='login'),
    path('login/welcome', views.welcome, name='welcome'),
    path('logout/', views.logoutView, name='logout'),
]
