from django.shortcuts import render, redirect
from .forms import MoodForm
from .models import PostMood
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
import json
import requests

def landing(request):
    mood_form = MoodForm(request.POST or None)
    if request.method == "POST":
        if mood_form.is_valid():
            mood_form.save()
            return redirect('/')
    else:
        mood_form = MoodForm()
            
    posts = PostMood.objects.all()
	
    contexts = {
        'posts': posts,
        'mood_form': mood_form,
    }
    
    return render(request, 'landing.html', contexts)

def books(request):
    return render(request, 'books.html')

def getJson(request, query):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + query
    json_data = json.loads(requests.get(url).text)
    return JsonResponse(json_data)

def loginView(request):
    if request.method == "POST":
        username_login = request.POST['username']
        password_login = request.POST['password']
        user = authenticate(request, username=username_login, password=password_login)
        if user is not None:
            login(request, user)
            return redirect('S6App:welcome')
        else:
            return redirect('S6App:login')

    return render(request, 'login.html')

def welcome(request):
    return render(request, 'welcome.html')

def logoutView(request):
    if request.method == "POST":
        print(request.POST)
        if request.POST["logout"] == "yes":
            logout(request)
        return redirect('S6App:landing')
    return render(request, 'logout.html')