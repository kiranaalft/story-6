from django import forms
from .models import PostMood

class MoodForm(forms.ModelForm):
	class Meta:
		model = PostMood
		fields = [
			'mood',
		]
		
		labels = {
			'mood':'Your mood rn: ',
		}
		
		widgets = {
			'mood':forms.TextInput(
				attrs = {
					'class':'form-control',
					'placeholder':'happy as a unicorn :D'
				}
			),
			'created_date':forms.DateTimeInput(
				attrs = {
					'class':'form-control',
				}
			),
		}