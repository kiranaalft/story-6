$(document).ready(function(){
	$('#search-button').click(function(){
		var query = $('#search-box').val();

		$(function(){
			$.ajax({
				url: "/books/getJson/" + query,
				success: function(result){
					$('#books-table').empty();		
				    $.each(result.items, function(i, item) {
				        $('<tr>').append(
				        	$('<th scope="row">').text((i+1)),
				        	$('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
				            $('<td>').text(item.volumeInfo.title),
				            $('<td>').text(item.volumeInfo.authors),
							$('<td>').append('<a href="' + item.volumeInfo.canonicalVolumeLink + '" target="_blank">&#x1f4dc;</a>')
				        ).appendTo('#books-table');
					});
				}
			});
		});
	});
	$('#search-box').val("r.h. sin");
	$('#search-button').click();
});
