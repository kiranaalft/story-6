from django.db import models
from datetime import datetime

# Create your models here.

class PostMood(models.Model):
	mood = models.CharField(max_length = 300)
	created_date = models.DateTimeField(auto_now_add = True)
	
	def __str__(self):
		return "{}.{}".format(self.id, self.mood)