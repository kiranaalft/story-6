from django.apps import AppConfig


class S6AppConfig(AppConfig):
    name = 'S6App'
